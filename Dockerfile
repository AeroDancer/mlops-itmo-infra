FROM python:3.12
WORKDIR /mlflow
COPY requirements.txt .
RUN pip install -r requirements.txt
ENTRYPOINT mlflow server \
    --backend-store-uri postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres:5432/${POSTGRES_DB} \
    --artifacts-destination s3://${MLFLOW_S3_BUCKET} \
    --host 0.0.0.0 \
    --port 5000